from distutils.core import setup
from Cython.Build import cythonize
from distutils.extension import Extension
from Cython.Distutils import build_ext

setup(
  name = "cython_multithread",
  cmdclass = {"build_ext": build_ext},
  ext_modules =
  [
    Extension("cython_multithread",
              ["cython_multithread.pyx"],
              extra_compile_args = ["-O2", "-fopenmp"],
              extra_link_args=['-fopenmp']
              )
  ]
)
