import python_singlethread
import cython_singlethread
import cython_multithread
import argparse

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("filepath1", type=str, default = -1, help="File path for word list 1")
	parser.add_argument("filepath2", type=str, default = -1, help="File path for word list 2")
	parser.add_argument("-scr", "--scriptType", type=str, default = "p-single", help="Which script to run? (p-single, c-single, c-multi)")
	parser.add_argument("-l1", "--limit1", type=int, default = -1, help="Limits the size of word list 1")
	parser.add_argument("-l2", "--limit2", type=int, default = -1, help="Limits the size of word list 2")
	args = parser.parse_args()

	if args.scriptType == "c-single":
		print("Running Cython singlethread\n")
		cython_singlethread.run(args.filepath1, args.filepath2, args.limit1, args.limit2)

	elif args.scriptType == "c-multi":
		print("Running Cython multithread\n")
		cython_multithread.run(args.filepath1, args.filepath2, args.limit1, args.limit2)

	else:
		print("Running Python singlethread\n")
		python_singlethread.run(args.filepath1, args.filepath2, args.limit1, args.limit2)
