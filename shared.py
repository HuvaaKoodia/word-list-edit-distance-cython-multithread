import os, sys, csv, time, math
from dateutil.relativedelta import relativedelta
csv.field_size_limit(sys.maxsize)

scriptPath = os.path.realpath(__file__)
scriptPath = scriptPath[:scriptPath.rfind(os.path.sep)]
outputPath = os.path.join(scriptPath, "Output")

def StartTimer():
	return time.time()

def EndTimer(startTime, showSeconds = False):
	rt = relativedelta(seconds=time.time() - startTime)
	if showSeconds:
		print("Done in {:02d}h {:02d}m {:02f}s".format(int(rt.hours), int(rt.minutes),rt.seconds))
	else:
		print("Done in {:02d}h {:02d}m".format(int(rt.hours), int(rt.minutes)))

def ProgressBar(i, l):
	l = math.ceil(l / 10)
	if i % l == 0:
		print(".", end="", flush=True)

def WriteBestMatchesToOutput(fileName, data):
	#Smaller distances first
	data = sorted(data, key = lambda x:x[0])

	#Removing duplicate words when distance is 0
	for i in range(len(data)):
		if data[i][0] == 0:
			data[i] = [0, data[i][1]]
		else:
			break

	WriteCSVToOutput(fileName, data)

def WriteCSVToOutput(fileName, data, quoting = csv.QUOTE_MINIMAL, verbose=True):
	#Output path
	if not os.path.isdir(outputPath):
		os.makedirs(outputPath)
	filePath = os.path.join(outputPath, fileName)

	#Writing
	f = open(filePath, "w")
	writer = csv.writer(f, quoting = quoting)
	writer.writerows(data)
	f.close()

	if verbose:
		print(f"File written to {filePath}")
