# cython: language_level=3
cimport cython
cimport openmp
from libc.math cimport log
from cython.parallel cimport prange

import numpy as np
import os, csv, multiprocessing, shared

@cython.boundscheck(False)
@cython.wraparound(False)
def run(filepath1, filepath2, limit1=-1, limit2=-1):
	print("Loading files")
	maxStrLen = 40+1
	cdef int threadCount = multiprocessing.cpu_count()

	def ReadCSV(filename, maxsize = -1):
		f = open(filename)
		reader = csv.reader(f)
		words = []
		numbers = []
		for r in reader:
			word = r[0].lower().strip()
			words.append(word)
			a = [0]*maxStrLen
			a[0] = len(word)
			for i in range(0, len(word)):
				a[i+1] = ord(word[i])
			numbers.append(a)

			maxsize -= 1
			if maxsize == 0:
				break

		return words, np.array(numbers, dtype="intc")

	d1, d1v = ReadCSV(filepath1, limit1)
	d2, d2v = ReadCSV(filepath2, limit2)
	cdef int[:,:] d1view = d1v
	cdef int[:,:] d2view = d2v

	cdef int lend1 = len(d1)
	cdef int lend2 = len(d2)
	print(f"Word list lengths: {lend1}, {lend2}")
	print("Thread count:", threadCount)

	#Vowels table
	cdef int[:] vowels = np.zeros(400, dtype="intc")
	vowels[ord("a")] = 1
	vowels[ord("e")] = 1
	vowels[ord("i")] = 1
	vowels[ord("o")] = 1
	vowels[ord("u")] = 1
	vowels[ord("y")] = 1
	vowels[ord("ä")] = 1
	vowels[ord("ö")] = 1

	#Cost setup
	cdef int mC = 0 #Match letters cost
	cdef int sC = 1 #Same letter type cost
	cdef int dC = 3 #Different letter type cost
	cdef int rC = 2 #Replace letter cost

	#Create cost matrises, one per thread
	cdef int[:,:,:] m = np.zeros((threadCount, maxStrLen, maxStrLen), dtype="intc")

	#Set initial edge values
	cdef int t, i
	for t in range(threadCount):
		for i in range(maxStrLen):
			m[t][i][0] = i * rC
			m[t][0][i] = i * rC

	print("\nCalculating edit distances")
	cdef int[:,:] bestMatches1 = np.array([[10000,0,0]]*lend1, dtype="intc")
	cdef int[:,:] bestMatches2 = np.array([[10000,0,0]]*lend2, dtype="intc")
	cdef int w1, w2, x, y, lenw1, lenw2, l1, l2, c1, c2, c3, threadI, best

	timer = shared.StartTimer()

	#Only C compatible code beyond this point
	with nogil:
		#Parallel loop with prange
		for w1 in prange(lend1, num_threads=threadCount, schedule='static'):
			threadI = openmp.omp_get_thread_num()
			for w2 in range(lend2):
				lenw1 = d1view[w1][0]+1
				lenw2 = d2view[w2][0]+1

				#Edit distance
				for y in range(1, lenw1):
					for x in range(1, lenw2):
						l1 = d1view[w1][y]
						l2 = d2view[w2][x]
						if l1 == l2:
							c1 = mC
						elif vowels[l1] == vowels[l2]:
							c1 = sC
						else:
							c1 = dC

						c1 = c1 + m[threadI][y-1][x-1]
						c2 = rC + m[threadI][y-1][x]
						c3 = rC + m[threadI][y][x-1]

						if c1 <= c2 and c1 <= c3:
							m[threadI][y][x] = c1
						elif c2 < c1 and c2 < c3:
							m[threadI][y][x] = c2
						else:
							m[threadI][y][x] = c3

				best = m[threadI][y][x]

				if best < bestMatches1[w1][0]:
					bestMatches1[w1][0] = best
					bestMatches1[w1][1] = w1
					bestMatches1[w1][2] = w2

				if best < bestMatches2[w2][0]:
					bestMatches2[w2][0] = best
					bestMatches2[w2][1] = w2
					bestMatches2[w2][2] = w1

	shared.EndTimer(timer, True)
	print("")

	#Write files
	array = []
	for d in bestMatches1:
		array.append([d[0], d1[d[1]], d2[d[2]]])
	shared.WriteBestMatchesToOutput("EditDistancesCythonMultithread_words1.csv", array)

	array = []
	for d in bestMatches2:
		array.append([d[0], d2[d[1]], d1[d[2]]])
	shared.WriteBestMatchesToOutput("EditDistancesCythonMultithread_words2.csv", array)
