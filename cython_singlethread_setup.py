from distutils.core import setup
from Cython.Build import cythonize
from distutils.extension import Extension
from Cython.Distutils import build_ext

setup(
	name = "cython_singlethread",
	cmdclass = {"build_ext": build_ext},
	ext_modules =
	[Extension("cython_singlethread",["cython_singlethread.pyx"])]
)
