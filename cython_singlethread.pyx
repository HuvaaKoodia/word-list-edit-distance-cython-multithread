# cython: language_level=3
import sys, csv, shared

def run(filepath1, filepath2, limit1 = -1, limit2 = -1):
	print("Loading files")
	f = open(filepath1)
	reader = csv.reader(f)
	d1 = [r[0].lower().strip() for r in reader]

	f = open(filepath2)
	reader = csv.reader(f)
	d2 = [r[0].lower().strip() for r in reader]

	if limit1 > 0:
		d1 = d1[:limit1]
	if limit2 > 0:
		d2 = d2[:limit2]

	cdef int lend1 = len(d1)
	cdef int lend2 = len(d2)
	print(f"Word list lengths: {lend1}, {lend2}")

	#Edit distance setup
	vowels = {"a", "e", "i", "o", "u", "y", "ä", "ö"}

	mC = 0 #Match letters cost
	sC = 1 #Same letter type cost
	dC = 3 #Different letter type cost
	rC = 2 #Replace letter cost

	#Create cost matrix
	maxStrLen = 40+1

	m = [] * maxStrLen
	for _ in range(maxStrLen):
		m.append([0]*maxStrLen)

	#Set initial edge values
	for i in range(maxStrLen):
		m[i][0] = i * rC
		m[0][i] = i * rC

	def EditDistance(a, b):
		cdef int x=1
		cdef int y=1
		cdef int lena = len(a)+1
		cdef int lenb = len(b)+1
		for y in range(1, lena):
			for x in range(1, lenb):
				l1 = a[y-1]
				l2 = b[x-1]
				if l1 == l2:
					cost = mC
				elif (l1 in vowels) is not (l2 in vowels):
					cost = dC
				else:
					cost = sC
				m[y][x] = min(cost + m[y-1][x-1], rC + m[y-1][x], rC + m[y][x-1])
		return m[y][x]

	print("\nCalculating edit distances")
	bestMatches1 = [[10000,0,0]]*lend1
	bestMatches2 = [[10000,0,0]]*lend2
	timer = shared.StartTimer()

	#Loops are faster with cdef variables
	cdef int w1, w2
	for w1 in range(lend1):
		for w2 in range(lend2):
			distance = EditDistance(d1[w1], d2[w2])
			if distance < bestMatches1[w1][0]:
				bestMatches1[w1] = [distance, d1[w1], d2[w2]]
			if distance < bestMatches2[w2][0]:
				bestMatches2[w2] = [distance, d2[w2], d1[w1]]
		shared.ProgressBar(w1, lend1)

	print("")
	shared.EndTimer(timer, True)
	print("")

	shared.WriteBestMatchesToOutput("EditDistancesCythonSinglethread_words1.csv", bestMatches1)
	shared.WriteBestMatchesToOutput("EditDistancesCythonSinglethread_words2.csv", bestMatches2)
