# Word list edit distance Cython multithread

Calculates the edit distance between words in long lists using Python and Cython.

Includes: 
- A native Python script (baseline, slow)
- A slightly optimized Cython script (~4 times faster)
- A fully optimized multithreaded Cython script (10-100+ times faster!)

## Requirements

- [Python 3](https://www.python.org/)
- [Cython](https://cython.org/)
- [Numpy](https://numpy.org/)

# Usage

Run the commands from *CompileCython.sh* first.

There are a few example files in the Data folder. Take a look.

The *run.py* script can be used to run any of the three scripts. By default the native Python script is used:
```python
python3 run.py Data/WordsFin2k.csv Data/WordsEng2k.csv
```
Use the *-scr* argument to change the script type to Cython singlethreaded:
```python
python3 run.py Data/WordsFin2k.csv Data/WordsEng2k.csv -scr c-single
```
or Cython multithreaded:
```python
python3 run.py Data/WordsFin2k.csv Data/WordsEng2k.csv -scr c-multi
```
The *-l1* and *-l2* arguments can be used to limit the amount of lines read from each word list. Useful for testing:
```python
python3 run.py Data/WordsFin93k.csv Data/WordsEng370k.csv -scr c-multi -l1 3000 -l2 12000
```
For a proper stress test throw everything at it:
```python
python3 run.py Data/WordsFin93k.csv Data/WordsEng370k.csv -scr c-multi
```
(This will take some hours to finish...)
